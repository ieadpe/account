package com.mv.inovation.Account.controller;

import java.util.List;

import javax.validation.Valid;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mv.innovation.Common.dto.AccountDTO;
import com.mv.inovation.Account.business.AccountService;
import com.mv.inovation.Account.converter.Converter;
import com.mv.inovation.Account.persistence.Account;

@RestController
@RequestMapping("/")
//@Slf4j
public class AccountController {

	private static final Logger logger = Logger.getLogger(AccountController.class.getName());

	@Autowired
	private Converter converter;

	@Autowired
	private AccountService accountService;

	@Autowired
	private Environment env;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<AccountDTO> createAccount(@Valid @RequestBody AccountDTO dto) {
		logger.infof("Controller.createAccount - Chamada REST criar conta: {}", dto);

		Account account = accountService.createAccount(
				converter.convertToEntity(dto));

		return new ResponseEntity<>(
				converter.convertToDTO(account), HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<AccountDTO> updateAccount(
			@PathVariable(value = "id") Long accountId
			, @Valid @RequestBody AccountDTO dto) {		
		logger.infof("Controller.updateAccount - Chamada REST atualizar conta de usuário: {}", accountId);

		Account account = accountService.updateAccount(
				accountId, converter.convertToEntity(dto));

		return new ResponseEntity<>(
				converter.convertToDTO(account), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<AccountDTO>> findAccount(
			@RequestParam("nome") String name,
			@RequestParam("login") String login,
			@RequestParam("email") String email) {
		logger.infof("Controller.findAccount - Chamada REST buscar contas. Paràmetros nome: {}, login: {}, email: {}", 
				name, login, email);

		List<Account> list = accountService.findAccount(name, login, email);

		return new ResponseEntity<>(
				converter.convertToDTOList(list), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = {"/obter"})
	public ResponseEntity<AccountDTO> getAccountByLogin(
			@RequestParam("login") String login) {
		logger.infof("Controller.findAccount - Chamada REST obter conta por login. Parâmetro login: {}", 
				login);

		Account account = accountService.getAccountByLogin(login);

		return new ResponseEntity<>(
				converter.convertToDTO(account), HttpStatus.OK);
	}

	// -------- Admin Area --------
	// This method should only be accessed by users with role of 'admin'
	// We'll add the logic of role based auth later
	@RequestMapping("/admin")
	public String homeAdmin() {
		return "Este é um exeplo de área administrativa do servico Account executando na porta: " + env.getProperty("local.server.port");
	}

}
