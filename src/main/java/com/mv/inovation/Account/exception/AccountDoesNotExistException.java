package com.mv.inovation.Account.exception;

public class AccountDoesNotExistException extends RuntimeException{
	
	private static final long serialVersionUID = -6178156203994870524L;

	public AccountDoesNotExistException(String message) {
		super(message);
	}

}
