package com.mv.inovation.Account.converter;

import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.stereotype.Component;

import com.mv.innovation.Common.dto.AccountDTO;
import com.mv.inovation.Account.persistence.Account;
import com.mv.inovation.Account.util.Util;

@Component
public class Converter {
	
	private static final Logger logger = Logger.getLogger(Converter.class.getName());
	
	public Account convertToEntity(AccountDTO dto) {
		logger.infof("Converter.convertToEntity - Conversão de objeto dto para entity: {}", dto);
		Account entity = new Account();
		
		entity.setEmail(dto.getEmail());
		entity.setLogin(dto.getLogin());
		entity.setName(dto.getNome());
		entity.setPassword(dto.getSenha());
		entity.setRole(dto.getRole());
		
		return entity;
	}
	
	public AccountDTO convertToDTO(Account entity) {
		logger.infof("Converter.converterParaEntity - Conversão de objeto entity para dto: {}", entity);
		AccountDTO dto = new AccountDTO();
		
		dto.setEmail(entity.getEmail());
		dto.setId(entity.getId());
		dto.setLogin(entity.getLogin());
		dto.setNome(entity.getName());
		dto.setSenha(entity.getPassword());
		dto.setRole(entity.getRole());
		
		return dto;
	}
	
	public List<AccountDTO> convertToDTOList(List<Account> entityList){
		List<AccountDTO> dtoList = new ArrayList<>();
		
		if(!Util.emptyList(entityList)) {
			for(Account entity: entityList) {
				dtoList.add(
						this.convertToDTO(entity));
			}
		}
		return dtoList;
	}

}
