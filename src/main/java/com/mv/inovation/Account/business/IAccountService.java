package com.mv.inovation.Account.business;

import java.util.List;

import com.mv.inovation.Account.persistence.Account;

public interface IAccountService {
	
	public Account createAccount(Account conta);
	
	public Account updateAccount(Long id, Account newAccount);
	
	public List<Account> findAccount(String name, String login, String email);
	
	public Account getAccountByLogin(String login);

}
