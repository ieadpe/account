package com.mv.inovation.Account.business;

import java.util.List;
import java.util.Optional;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.mv.inovation.Account.exception.AccountDoesNotExistException;
import com.mv.inovation.Account.persistence.Account;
import com.mv.inovation.Account.persistence.AccountRepository;
import com.mv.inovation.Account.persistence.AccountSpecification;
import com.mv.inovation.Account.persistence.SearchCriteria;

@Service
public class AccountService implements IAccountService{
	
	private static final String DOIS_PONTOS = ":";

	private static final Logger logger = Logger.getLogger(AccountService.class.getName());
	
	@Autowired
	private AccountRepository contaRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public Account createAccount(Account conta) {
		logger.infof("ContaService.createAccount - Camada de servico para persistencia de objeto conta: {}", conta);
		logger.infof("ContaService.criarConta - Encriptando senha...");
		prepareToPersist(conta);
		logger.infof("ContaService.createAccount - Persistindo objeto conta: {}", conta);
		return contaRepository.save(conta);
	}

	@Override
	public Account updateAccount(Long id, Account newAccount) {
		logger.infof("ContaService.updateAccount - Camada de servico para atualizar objeto conta: {}", id);
		
		Optional<Account> recoveredAccount = contaRepository.findById(id);
		
		if(!recoveredAccount.isPresent()) {
			logger.errorf("ContaService.updateAccount - Não foi possível identificar a conta de idntifcação: {}", id);
			throw new AccountDoesNotExistException("Não foram enontrados resultados para a consulta solicitada");
		}
		
		Account accountToUpdate = recoveredAccount.get();
		accountToUpdate.setEmail(newAccount.getEmail());
		
		return contaRepository.save(accountToUpdate);
	}

	@Override
	public List<Account> findAccount(String name, String login, String email) {
		logger.infof("ContaService.findAccount - Camada de servico para buscar objetos conta");
		
		AccountSpecification specName = 
			      new AccountSpecification(new SearchCriteria("name", DOIS_PONTOS, name.toLowerCase()));
		AccountSpecification specLogin = 
			      new AccountSpecification(new SearchCriteria("login", DOIS_PONTOS, login.toLowerCase()));
		AccountSpecification specEmail = 
			      new AccountSpecification(new SearchCriteria("email", DOIS_PONTOS, email.toLowerCase()));
			    
		return contaRepository.findAll(
				Specification.where(specName).and(specLogin).and(specEmail));
	}
	
	@Override
	public Account getAccountByLogin(String login) {
		logger.infof("ContaService.getAccountByLogin - Camada de servico para obter conta por login");
		Account account = contaRepository.findByLogin(login);
		
		if(account == null) {
			throw new AccountDoesNotExistException("A conta solicitada não foi encontrada");
		}
		return account;
	}
	
	private void prepareToPersist(Account conta) {
		encryptPassword(conta);
		// all users created by call method are USER
		conta.setRole("USER");
	}
	
	private void encryptPassword(Account account) {
		if(account != null 
				&& !account.getPassword().isEmpty()) {
			
			account.setPassword(
					passwordEncoder.encode(
							account.getPassword()));
		}
	}
	
	@Bean
	private PasswordEncoder passwordEncoder() {
	    return new BCryptPasswordEncoder();
	}

}
