package com.mv.inovation.Account.persistence;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class AccountSpecification implements Specification<Account> {

	private static final long serialVersionUID = 1876051407018842681L;

	private static final String PERCENT = "%";

	private SearchCriteria criteria;

	public AccountSpecification(SearchCriteria criteria) {
		this.criteria = criteria;
	}

	@Override
	public Predicate toPredicate(Root<Account> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		return criteriaBuilder.like(
				criteriaBuilder.lower(
						root.<String>get(criteria.getKey()))
				, PERCENT + criteria.getValue() + PERCENT);
	}

}
