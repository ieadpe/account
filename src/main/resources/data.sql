INSERT INTO Conta (nome, login, email, senha, role) VALUES
  ('Darth Vader', 'lordVader', 'DarthVader@imperio.com', '$2a$10$I/NuDcQjpIDRBXF2ezFQnuYjbozq0cgioGSXBT2cYFe3XnsCUuzSa', 'USER'),
  ('Anakin Skywalker', 'Anakin', 'anakinm_jedi@jedi.com', '$2a$10$/ZHjUiXZoz6iJiWs/cxu8uEVAuJW/X448AkZqNGA0vZN7tNE.ErTm', 'ADMIN'),
  ('Luke Skywalker', 'lukeSky', 'ultimoJedi@mail.com', '$2a$10$oUuwkwJHpsiQNKBmOKbAy.4t7o2HomqtqKqYSZC4gJYJr8DLMXoy2', 'USER'),
  ('Mestre Yoda', 'yoda', '', '$2a$10$S9MIbViJDapgAtE8S6pwu.XCP6TiEdjF8qql0apvI8qSuARWV2E4i', 'ADMIN');